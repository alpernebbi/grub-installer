# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Italian messages for debian-installer.
# Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2017 Software in the Public Interest, Inc.
# Copyright (C) 2018 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# The translation team (for all four levels):
# Cristian Rigamonti <cri@linux.it>
# Danilo Piazzalunga <danilopiazza@libero.it>
# Davide Meloni <davide_meloni@fastwebnet.it>
# Davide Viti <zinosat@tiscali.it>
# Filippo Giunchedi <filippo@esaurito.net>
# Giuseppe Sacco <eppesuig@debian.org>
# Lorenzo 'Maxxer' Milesi
# Renato Gini
# Ruggero Tonelli
# Samuele Giovanni Tonon <samu@linuxasylum.net>
# Stefano Canepa <sc@linux.it>
# Stefano Melchior <stefano.melchior@openlabs.it>
# Luca Monducci <luca.mo@tiscali.it>
#
#
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@computer.org>, 2001
# Alessio Frusciante <algol@firenze.linux.it>, 2001
# Andrea Scialpi <solopec@tiscalinet.it>, 2001
# (translations from drakfw)
# Copyright (C) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011 Free Software Foundation, Inc.
# Danilo Piazzalunga <danilopiazza@libero.it>, 2004
# Davide Viti <zinosat@tiscali.it>, 2006
# Marcello Raffa <mrooth@tiscalinet.it>, 2001
# Tobias Toedter <t.toedter@gmx.net>, 2007.
# Translations taken from ICU SVN on 2007-09-09
# Milo Casagrande <milo@milo.name>, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2017, 2018, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: grub-installer@packages.debian.org\n"
"POT-Creation-Date: 2023-09-14 20:02+0000\n"
"PO-Revision-Date: 2020-10-26 09:14+0100\n"
"Last-Translator: Milo Casagrande <milo@milo.name>\n"
"Language-Team: Italian <tp@lists.linux.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:1001 ../grub-installer.templates:2001
msgid "Install the GRUB boot loader to your primary drive?"
msgstr "Installare il boot loader GRUB sul disco primario?"

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:1001 ../grub-installer.templates:34001
msgid ""
"The following other operating systems have been detected on this computer: "
"${OS_LIST}"
msgstr ""
"I seguenti sistemi operativi sono stati rilevati su questo computer: "
"${OS_LIST}"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:1001
msgid ""
"If all of your operating systems are listed above, then it should be safe to "
"install the boot loader to your primary drive (UEFI partition/boot record). "
"When your computer boots, you will be able to choose to load one of these "
"operating systems or the newly installed Debian system."
msgstr ""
"Se tutti i sistemi operativi sono elencati sopra, è allora possibile "
"installare in sicurezza il boot loader sul disco primario (partizione/boot "
"record UEFI). All'avvio del computer, sarà possibile caricare uno di questi "
"sistemi operativi oppure il nuovo sistema Debian."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:2001
msgid ""
"It seems that this new installation is the only operating system on this "
"computer. If so, it should be safe to install the GRUB boot loader to your "
"primary drive (UEFI partition/boot record)."
msgstr ""
"Sembra che questa nuova installazione sia l'unico sistema operativo presente "
"su questo computer. Se così fosse, è buona norma installare il boot loader "
"GRUB sul disco primario (partizione/boot record UEFI)."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:2001
msgid ""
"Warning: If your computer has another operating system that the installer "
"failed to detect, this will make that operating system temporarily "
"unbootable, though GRUB can be manually configured later to boot it."
msgstr ""
"Attenzione: se il computer dispone di un altro sistema operativo che il "
"programma di installazione non è riuscito a rilevare, tale sistema non sarà "
"temporaneamente disponibile. Successivamente, modificare GRUB manualmente "
"per poterlo avviare."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "Install the GRUB boot loader to the multipath device?"
msgstr "Installare il boot loader GRUB nel device multi-percorso?"

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "Installation of GRUB on multipath is experimental."
msgstr "L'installazione di GRUB su un sistema multi-percorso è sperimentale."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid ""
"GRUB is always installed to the master boot record (MBR) of the multipath "
"device. It is also assumed that the WWID of this device is selected as boot "
"device in the system's FibreChannel adapter BIOS."
msgstr ""
"GRUB viene sempre installato nel master boot record (MBR) del device multi-"
"percorso. Si assume che il WWID del device sia selezionato come device di "
"avvio nell'adattatore FibreChannel del BIOS."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "The GRUB root device is: ${GRUBROOT}."
msgstr "Il dispositivo root di GRUB è: ${GRUBROOT}."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:4001 ../grub-installer.templates:13001
msgid "Unable to configure GRUB"
msgstr "Impossibile configurare GRUB"

#. Type: error
#. Description
#. :sl3:
#: ../grub-installer.templates:4001
msgid "An error occurred while setting up GRUB for the multipath device."
msgstr ""
"Si è verificato un errore nel configurare GRUB per il device multi-percorso."

#. Type: error
#. Description
#. :sl3:
#: ../grub-installer.templates:4001
msgid "The GRUB installation has been aborted."
msgstr "L'installazione di GRUB è stata interrotta."

#. Type: string
#. Description
#. :sl2:
#. Type: select
#. Description
#. :sl2:
#: ../grub-installer.templates:5001 ../grub-installer.templates:6002
msgid "Device for boot loader installation:"
msgstr "Device per l'installazione del boot loader:"

#. Type: string
#. Description
#. :sl2:
#. Type: select
#. Description
#. :sl2:
#: ../grub-installer.templates:5001 ../grub-installer.templates:6002
msgid ""
"You need to make the newly installed system bootable, by installing the GRUB "
"boot loader on a bootable device. The usual way to do this is to install "
"GRUB to your primary drive (UEFI partition/boot record). You may instead "
"install GRUB to a different drive (or partition), or to removable media."
msgstr ""
"Bisogna rendere avviabile il nuovo sistema installato, installando il boot "
"loader GRUB nel dispositivo di avvio. Normalmente, si installa GRUB sul "
"disco primario (partizione/boot record UEFI). È possibile installare GRUB su "
"un altro disco (o partizione) oppure su un supporto rimovibile."

#. Type: string
#. Description
#. :sl2:
#: ../grub-installer.templates:5001
msgid ""
"The device notation should be specified as a device in /dev. Below are some "
"examples:\n"
" - \"/dev/sda\" will install GRUB to your primary drive (UEFI partition/"
"boot\n"
"   record);\n"
" - \"/dev/sdb\" will install GRUB to a secondary drive (which may for "
"instance\n"
"   be a thumbdrive);\n"
" - \"/dev/fd0\" will install GRUB to a floppy."
msgstr ""
"Il device dovrebbe essere specificato come un device in /dev. Ecco alcuni "
"esempi:\n"
" - «/dev/sda» installerà GRUB sul supporto primario (partizione/boot record "
"UEFI)\n"
" - «/dev/sdb» installerà GRUB su un supporto secondario (che potrebbe essere "
"una\n"
"   chiavetta USB)\n"
" - «/dev/fd0» installerà GRUB su dischetto."

#. Type: select
#. Choices
#: ../grub-installer.templates:6001
msgid "Enter device manually"
msgstr "Inserire il device manualmente"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:7001
msgid "GRUB password:"
msgstr "Password di GRUB:"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:7001
msgid ""
"The GRUB boot loader offers many powerful interactive features, which could "
"be used to compromise your system if unauthorized users have access to the "
"machine when it is starting up. To defend against this, you may choose a "
"password which will be required before editing menu entries or entering the "
"GRUB command-line interface. By default, any user will still be able to "
"start any menu entry without entering the password."
msgstr ""
"Se un utente non autorizzato avesse accesso fisico al computer durante "
"l'avvio del sistema, potrebbe utilizzare le molte e potenti caratteristiche "
"interattive del boot loader GRUB per compromettere il sistema. Per "
"proteggersi da questa eventualità, è possibile impostare una password che "
"verrà richiesta prima di modificare le voci del menù e anche prima di "
"attivare l'interfaccia a riga di comando. Qualsiasi utente avrà la "
"possibilità di selezionare una qualunque delle voci di menù, senza inserire "
"alcuna password."

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:7001
msgid "If you do not wish to set a GRUB password, leave this field blank."
msgstr "Per non impostare una password di GRUB, lasciare il campo vuoto."

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:8001
msgid "Re-enter password to verify:"
msgstr "Inserire nuovamente la password per verifica:"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:8001
msgid ""
"Please enter the same GRUB password again to verify that you have typed it "
"correctly."
msgstr ""
"Inserire nuovamente la stessa password di GRUB per verificare che sia stata "
"digitata correttamente."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:9001
msgid "Password input error"
msgstr "Errore nell'inserire la password"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:9001
msgid "The two passwords you entered were not the same. Please try again."
msgstr "Le due password inserite non sono uguali; riprovare."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:11001
msgid "GRUB installation failed"
msgstr "Installazione di GRUB non riuscita"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:11001
msgid ""
"The '${GRUB}' package failed to install into /target/. Without the GRUB boot "
"loader, the installed system will not boot."
msgstr ""
"L'installazione del pacchetto «${GRUB}» in /target/ non è riuscita. Senza il "
"boot loader GRUB, il sistema installato non si avvierà."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:12001
msgid "Unable to install GRUB in ${BOOTDEV}"
msgstr "Impossibile installare GRUB su ${BOOTDEV}"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:12001
msgid "Executing 'grub-install ${BOOTDEV}' failed."
msgstr "Esecuzione di «grub-install ${BOOTDEV}» non riuscita."

#. Type: error
#. Description
#. :sl2:
#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:12001 ../grub-installer.templates:13001
msgid "This is a fatal error."
msgstr "Questo è un errore fatale."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:13001
msgid "Executing 'update-grub' failed."
msgstr "Esecuzione di «update-grub» non riuscita."

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid "Install GRUB?"
msgstr "Installare GRUB?"

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid ""
"GRUB 2 is the next generation of GNU GRUB, the boot loader that is commonly "
"used on i386/amd64 PCs. It is now also available for ${ARCH}."
msgstr ""
"GRUB 2 è la nuova generazione di GNU GRUB, il boot loader maggiormente "
"utilizzato su computer i386/amd64 e disponibile ora anche per ${ARCH}."

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid ""
"It has interesting new features but is still experimental software for this "
"architecture. If you choose to install it, you should be prepared for "
"breakage, and have an idea on how to recover your system if it becomes "
"unbootable. You're advised not to try this in production environments."
msgstr ""
"Dispone di nuove interessanti caratteristiche, ma è ancora considerato "
"sperimentale per questa architettura. Se viene installato, si potrebbero "
"riscontrare malfunzionamenti ed è necessario conoscere come poter "
"ripristinare il sistema nel caso non si riesca ad avviare. Non è consigliato "
"utilizzarlo in ambienti di produzione."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:16001
msgid "Installing GRUB boot loader"
msgstr "Installazione del boot loader GRUB"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:17001
msgid "Looking for other operating systems..."
msgstr "Ricerca di altri sistemi operativi..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:18001
msgid "Installing the '${GRUB}' package..."
msgstr "Installazione del pacchetto «${GRUB}»..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:19001
msgid "Determining GRUB boot device..."
msgstr "Determinazione del device di avvio di GRUB..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:20001
msgid "Running \"grub-install ${BOOTDEV}\"..."
msgstr "Esecuzione di «grub-install ${BOOTDEV}»..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:21001
msgid "Running \"update-grub\"..."
msgstr "Esecuzione di «update-grub»..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:22001
msgid "Updating /etc/kernel-img.conf..."
msgstr "Aggiornamento di /etc/kernel-img.conf..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:23001
msgid "Checking whether to force usage of the removable media path"
msgstr "Verifica se forzare l'uso del percorso del dispositivo rimovibile"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:24001
msgid "Mounting filesystems"
msgstr "Montaggio dei file system"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:25001
msgid "Configuring grub-efi for future usage of the removable media path"
msgstr ""
"Configurazione grub-efi per il futuro utilizzo del percorso del supporto "
"rimovibile"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../grub-installer.templates:26001
msgid "Install the GRUB boot loader"
msgstr "Installare il boot loader GRUB"

#. Type: text
#. Description
#. Rescue menu item
#. :sl2:
#: ../grub-installer.templates:27001
msgid "Reinstall GRUB boot loader"
msgstr "Re-installare il boot loader GRUB"

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
#, fuzzy
msgid "Failed to mount ${PATH}"
msgstr "Montaggio del dischetto non riuscito"

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
#, fuzzy
msgid "Mounting the ${FSTYPE} file system on ${PATH} failed."
msgstr "Il montaggio del file system proc su /target/proc non è riuscito."

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr "Controllare /var/log/syslog o la console virtuale 4 per i dettagli."

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
msgid "Warning: Your system may be unbootable!"
msgstr "Attenzione: il sistema potrebbe non essere avviabile."

#. Type: text
#. Description
#. Rescue menu item
#. :sl2:
#: ../grub-installer.templates:31001
msgid "Force GRUB installation to the EFI removable media path"
msgstr ""
"Forzare l'installazione di GRUB sul percorso del dispositivo rimovibile EFI"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid "Force GRUB installation to the EFI removable media path?"
msgstr ""
"Forzare l'installazione di GRUB sul percorso del dispositivo rimovibile EFI?"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid ""
"It seems that this computer is configured to boot via EFI, but maybe that "
"configuration will not work for booting from the hard drive. Some EFI "
"firmware implementations do not meet the EFI specification (i.e. they are "
"buggy!) and do not support proper configuration of boot options from system "
"hard drives."
msgstr ""
"Sembra che questo computer sia configurato per avviarsi tramite EFI, ma "
"quella configurazione potrebbe non funzionare per l'avvio da disco rigido. "
"Alcune implementazioni del firmware EFI non sono conformi alle specifiche "
"EFI (contengono bug) e non supportano la corretta configurazione delle "
"opzioni di avvio dai dischi di sistema."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid ""
"A workaround for this problem is to install an extra copy of the EFI version "
"of the GRUB boot loader to a fallback location, the \"removable media path"
"\". Almost all EFI systems, no matter how buggy, will boot GRUB that way."
msgstr ""
"Una soluzione per questo problema consiste nell'installare una copia "
"aggiuntiva della versione EFI del boot loader GRUB in una posizione di "
"ripiego, il \"percorso del supporto rimovibile\". Quasi tutti i sistemi EFI, "
"per quanti bug possano avere, sono in grado di avviare GRUB in quel modo."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid ""
"Warning: If the installer failed to detect another operating system that is "
"present on your computer that also depends on this fallback, installing GRUB "
"there will make that operating system temporarily unbootable. GRUB can be "
"manually configured later to boot it if necessary."
msgstr ""
"Attenzione: se il programma d'installazione non riesce a riconoscere un "
"altro sistema operativo presente su questo computer che dipende da questo "
"ripiego, installarvi GRUB renderà il sistema operativo temporaneamente non "
"avviabile. Se necessario, GRUB può essere configurato successivamente per "
"avviarlo."

#. Type: boolean
#. Description
#. Same as grub2/update_nvram
#. :sl1:
#: ../grub-installer.templates:33001
msgid "Update NVRAM variables to automatically boot into Debian?"
msgstr "Aggiornare le variabili NVRAM per avviare automaticamente Debian?"

#. Type: boolean
#. Description
#. Same as grub2/update_nvram
#. :sl1:
#: ../grub-installer.templates:33001
msgid ""
"GRUB can configure your platform's NVRAM variables so that it boots into "
"Debian automatically when powered on. However, you may prefer to disable "
"this behavior and avoid changes to your boot configuration. For example, if "
"your NVRAM variables have been set up such that your system contacts a PXE "
"server on every boot, this would preserve that behavior."
msgstr ""
"GRUB può configurare le variabili NVRAM della propria piattaforma in modo da "
"avviare automaticamente Debian all'accensione. Tuttavia, si potrebbe non "
"volere questo comportamento ed evitare modifiche alla propria configurazione "
"d'avvio. Per esempio, se le variabili NVRAM sono state impostate in modo che "
"il sistema contatti un server PXE a ogni avvio, è possibile preservare tale "
"impostazione."

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:34001 ../grub-installer.templates:35001
msgid "Run os-prober automatically to detect and boot other OSes?"
msgstr ""

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:34001 ../grub-installer.templates:35001
msgid ""
"GRUB can use the os-prober tool to attempt to detect other operating systems "
"on your computer and add them to its list of boot options automatically."
msgstr ""

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:34001 ../grub-installer.templates:35001
msgid ""
"If your computer has multiple operating systems installed, then this is "
"probably what you want. However, if your computer is a host for guest OSes "
"installed via LVM or raw disk devices, running os-prober can cause damage to "
"those guest OSes as it mounts filesystems to look for things."
msgstr ""

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:35001
msgid ""
"os-prober did not detect any other operating systems on your computer at "
"this time, but you may still wish to enable it in case you install more in "
"the future."
msgstr ""
